package com.jenkins.practica;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Test;

import com.jenkins.utils.DogsOperations;

public class OperationsTests {

	/**
	 * Checks if the operation getRandomDogImage returns a .jpg
	 */
	@Test
	public void testRandom() {
		// Instantiate DogsOperations class
		DogsOperations Dogs = new DogsOperations();
		// Call getRandomDogImage operation and store the result on String
		String MyString = Dogs.getRandomDogImage();
		System.out.print(MyString);
		// Assert true if the result string ends with '.jpg'
		assertTrue(MyString.endsWith(".jpg"));
	}
	
	/**
	 * Checks if the operation getBreedList returns a list of dogs (f.e. size > 0)
	 */
	@Test
	public void breedsList() {
		// Instantiate DogsOperations class
		DogsOperations Dogs = new DogsOperations();
		// Call getBreedList operation and store the result on ArrayList
		ArrayList<String> BreedList = Dogs.getBreedList();
		// Assert true if the result ArrayList has size of more than 0
		assertTrue(BreedList.size()>0);
	}
}
